﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace ExchangeFromJson
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            #region Try
            //WebClient client = new WebClient();
            //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            //string jsonString = client.DownloadString("https://data.egov.kz/api/v2/valutalar_bagamdary4/v571");
            ////string xmlString = client.DownloadString("http://nationalbank.kz/?getpg=outurl&out=https://nationalbank.kz/rss/rates_all.xml");
            //var exchanges = ExchangeJSON.FromJson(jsonString);
            //var info = JsonConvert.DeserializeObject<ExchangeJSON>(jsonString);
            //client.Dispose();


            //var doc = new XmlDocument();
            //doc.Load("https://nationalbank.kz/rss/rates_all.xml?switch=russian");

            //var item = doc.FirstChild;
            //var title = item["pubDate"].InnerText;
            //var description = item["description"].InnerText;
            //var quant = item["quant"].InnerText;
            //var index = item["index"].InnerText;
            //var change = item["change"].InnerText;
            //var link = item["link"].InnerText;

            //var rss = doc.FirstChild;
            //var channel = rss["channel"].InnerText;
            ////var chTitle = channel["channel"].InnerText;

            //XmlElement root = doc.DocumentElement;
            //foreach(XmlNode node in root)
            //{

            //}

            #endregion
        }

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            string xmlFile = @"https://nationalbank.kz/rss/rates_all.xml?switch=russian";
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(xmlFile);
            DataView dataView = new DataView(dataSet.Tables[2]);
            dataView.Table.Columns[0].ColumnName = "Название";
            dataView.Table.Columns[1].ColumnName = "Дата";
            dataView.Table.Columns[2].ColumnName = "Курс";
            dataView.Table.Columns[3].ColumnName = "Соотношение";
            dataView.Table.Columns[4].ColumnName = "Индекс";
            dataView.Table.Columns[5].ColumnName = "Изменения";
            dataGrid.ItemsSource = dataView;
        }
    }
}
